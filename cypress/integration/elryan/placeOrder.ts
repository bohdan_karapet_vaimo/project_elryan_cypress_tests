const sites = [
    'https://www.elryan.com/en',
    'https://www.elryan.com/ar'
];
let links = [];

describe('Order placement', () => {
    it('Add to cart and go to checkout order review page', () => {
        for (let i = 0; i < sites.length; i++) {
            // open home page
            cy.visit(sites[i]);

            // open first category
            cy.get('.top-menu-link')
                .first()
                .click();

            // wait till category banner is visible
            cy.get('.category-banner__main', { timeout: 10000 })
                .should('be.visible');
            cy.wait(1000);

            // add first available product to cart
            cy.get('.add-to-cart button', { timeout: 10000 })
                .eq(8).should('not.be.disabled')
                .first()
                .scrollIntoView()
                .should('be.visible')
                .click({force: true});

            // wait 4s till minicart opens
            cy.wait(4000);

            // go to cart page
            cy.get('.microcart .actions-btn a[data-testid="subscribeSubmit"]', { timeout: 10000 })
                .click();
            cy.wait(2000);

            // go to checkout
            cy.get('.order-info .summary + a[data-testid="subscribeSubmit"]', { timeout: 10000 })
                .click();

            // enter mobile number
            cy.get('.input-tel input')
                .type('7912345678', {force: true});
            cy.wait(2000);

            // enter password
            cy.get('.password-input input[type=password]')
                .type('T@t123', {force: true});
            cy.wait(2000);

            // click on continue
            cy.get('.personal-details .button-container button')
                .click({force: true});

            // click on continue
            cy.get('button[data-testid="shippingSubmit"]')
                .click({force: true});

            // choose cash on delivery
            cy.get('.payment-method .cash')
                .click({force: true});


            // click on continue
            cy.get('button[data-testid="paymentSubmit"]')
                .click({force: true});

            // clear local storage before next session
            cy.clearLocalStorage();
        }
    })
})
