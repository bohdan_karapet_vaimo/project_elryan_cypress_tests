# README #

### What is this repository for? ###

This is repository for Cypress autotests on ElRyan project.

### How do I get set up? ###

* clone repo
* run `yarn install`
* [optional] run `tsc -w` to run typescript in watch mode (Cypress can run TS files without compilation)
* run `yarn cypress` to start testing
